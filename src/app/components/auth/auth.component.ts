import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/service/auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent  implements OnInit{
  Authstatus!:boolean;
  ngOnInit(){
   this.Authstatus=this.authservice.isAuth;
  }
  constructor(private authservice:AuthService,private router:Router){

  }
  ONsignIn(){
    this.authservice.SignIn().then(
      ()=>{
        this.Authstatus=this.authservice.isAuth;
        this.router.navigate(['appareils']);
      }
    )
  }
  ONsignOut(){
  this.authservice.SignOut();
  this.Authstatus=this.authservice.isAuth;
 }


}
