import { Component, Input, OnInit } from '@angular/core';
import { AppareilService } from 'src/app/service/appareil.service';

@Component({
  selector: 'app-appareils',
  templateUrl: './appareils.component.html',
  styleUrls: ['./appareils.component.css']
})
export class AppareilsComponent implements OnInit {
  @Input()
  AppareilName!: string;

  constructor(private appareilservice:AppareilService){

  }
  @Input()
  AppareilStatut!:string;

  @Input()
  indexOfAppareil!: number;
  @Input()
  id!:number
  
  ngOnInit(): void {
  }

  getStatut(){
    return this.AppareilStatut;
  }
  getColor(){
    if(this.AppareilStatut === 'eteint'){
     return 'red'
    }else{
    return 'green'
    }

  }

  onSwitchOn(){
   this.appareilservice.switchOnOne(this.indexOfAppareil);
  }
  onSwicthOff(){
    this.appareilservice.switchOffOne(this.indexOfAppareil);
  }

}
