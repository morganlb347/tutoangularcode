import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { user } from 'src/app/modele/user.model';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.css']
})
export class UserlistComponent implements OnInit,OnDestroy {
  users:user[]=[];
  usersubcription!:Subscription;

  ngOnInit(){
  this.usersubcription= this.userservice.userSubject.subscribe(
    (users:user[])=>{
      this.users=users;
    }
  );
  this.userservice.emitUsers();
  }

  constructor( private userservice:UserService){
  }
  ngOnDestroy(){
   this.usersubcription.unsubscribe();
  }

}
