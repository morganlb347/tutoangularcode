import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppareilService } from 'src/app/service/appareil.service';

@Component({
  selector: 'app-singleappareil',
  templateUrl: './singleappareil.component.html',
  styleUrls: ['./singleappareil.component.css']
})
export class SingleappareilComponent implements OnInit {
  name:String="appareil";
  status:String="eteint";
  ngOnInit() {
    const id=this.route.snapshot.params['id'];
    this.name=this.appareilservice.getAppareilByid(+id)!.name;
    this.status=this.appareilservice.getAppareilByid(+id)!.status;


  }

  constructor(private appareilservice: AppareilService,private route:ActivatedRoute){

  }

}
