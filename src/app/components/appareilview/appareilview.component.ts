import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AppareilService } from 'src/app/service/appareil.service';

@Component({
  selector: 'app-appareilview',
  templateUrl: './appareilview.component.html',
  styleUrls: ['./appareilview.component.css']
})
export class AppareilviewComponent  implements OnInit {


  title = 'testmonprojectangular';
  isAuth=false;
  lastUpdate = new Promise<Date>(
    (resolve,reject)=>{
      const date =new Date();
      setTimeout(
        () =>{
          resolve(date);
        },2000
      )
    }
  );

  appareils!: any[];
  appareilsubcription!:Subscription;



  constructor(private appareilService:AppareilService){
   setTimeout(() => {
    this.isAuth=true
   }, 4000);
  }
  ngOnInit(): void {
    this.appareilsubcription=this.appareilService.appareilSubject.subscribe(
      (appareils:any[])=>{
        this.appareils=appareils;
      }
    );
    this.appareilService.emitappareilsubject();
    this.onFetch();
  }


  onAllumer(){
    this.appareilService.switchOnAll();
  }
  onEteindre() {
    this.appareilService.switchOffAll();
    }

  onSave(){
    this.appareilService.saveAppareiltoserver();
  }
  onFetch(){
    this.appareilService.getAppareilfromserveur();
  }



}
