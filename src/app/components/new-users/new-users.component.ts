import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { user } from 'src/app/modele/user.model';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-new-users',
  templateUrl: './new-users.component.html',
  styleUrls: ['./new-users.component.css']
})
export class NewUsersComponent implements OnInit{
  userform!:FormGroup;
  firstname:FormControl=new FormControl('',Validators.required);
  lastname:FormControl=new FormControl('',Validators.required);
  email:FormControl=new FormControl('',Validators.required);

  drinkpreference=new FormControl('',Validators.required);

  ngOnInit(){
    this.email.addValidators(Validators.email);
    this.initform();
  }
  constructor(private formbuilder:FormBuilder,
    private userservice:UserService,
    private router:Router){}

  initform(){
   this.userform=this.formbuilder.group({
    firstname:this.firstname,
    lastname:this.lastname,
    email:this.email,
    drinkpreference:this.drinkpreference,
    hobbies:this.formbuilder.array([])
   });
  }


  onSubmitForm(){
    const formvalue=this.userform.value;
    const newUser=new user(
      formvalue['firstname'],
      formvalue['lastname'],
      formvalue['email'],
      formvalue['drinkpreference'],
      formvalue['hobbies'] ? formvalue['hobbies'] :[]
      );
    this.userservice.addUser(newUser);
    this.router.navigate(['/users']);
  }

   getHobbies(){
    return this.userform.get('hobbies') as FormArray;
   }

   onAddHobbies(){
    const newHobbyControl=this.formbuilder.control('',Validators.required);
    this.getHobbies().push(newHobbyControl);
   }
}
