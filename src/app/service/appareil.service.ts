import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AppareilService {
  appareilSubject=new Subject<any[]>();

  constructor(private httpClient:HttpClient) { }
  private appareils=[{
  id:1,
  name:'machine a laver',
  status:'eteint'
   }];

emitappareilsubject(){
  this.appareilSubject.next(this.appareils.slice());

}

getAppareilByid(id : number){
  const appareil=this.appareils.find(
    (appareilsObject)=> {
    return appareilsObject.id === id;
    }
  );
  return appareil;
}

switchOnAll(){
 for(let appareil of this.appareils){
  appareil.status='allumer'
 }
 this.emitappareilsubject();
}

switchOffAll(){
  for(let appareil of this.appareils){
    appareil.status='eteint'
   }
   this.emitappareilsubject();
}
switchOnOne(index: number){
  this.appareils[index].status='allumer';
  this.emitappareilsubject();
}

switchOffOne(index: number){
  this.appareils[index].status='eteint';
  this.emitappareilsubject();
}

addAppareil(name:string,status:string){
 const appareilsObject={
  id:0,
  name:'',
  status:''
 };
 appareilsObject.name=name;
 appareilsObject.status=status;
 appareilsObject.id=this.appareils[(this.appareils.length-1)].id+1;

 this.appareils.push(appareilsObject);
 this.emitappareilsubject();
}
 saveAppareiltoserver(){
  this.httpClient
  .put("https://http-client-demo-e1cb9-default-rtdb.europe-west1.firebasedatabase.app/appareils.json",this.appareils)
  .subscribe(
    ()=>
    {
    console.log("enregistrement terminée");
    },
    (error)=>
    {
     console.log("erreur dans enregistrement"+error);
    }


  );
 }

 getAppareilfromserveur(){
  this.httpClient
   .get<any[]>("https://http-client-demo-e1cb9-default-rtdb.europe-west1.firebasedatabase.app/appareils.json")
   .subscribe(
      (Response)=>{
        this.appareils=Response;
        this.emitappareilsubject();
      },
      (error)=>{
        console.log("erreur dans la recupération"+error);
      }
   );
 }



}
