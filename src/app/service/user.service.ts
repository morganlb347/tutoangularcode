import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { user } from '../modele/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private users:user[]=[
    {
      firstname:'jean',
      lastname:'michel',
      email:'test@tttt.com',
      drinkpreference:'cocaine',
      hobbies:['fumer','boire']
    }
  ];
  
  userSubject=new Subject<user[]>;

   emitUsers(){
    this.userSubject.next(this.users.slice());
   }
    addUser(user:user){
    this.users.push(user);
    this.emitUsers();
    }

  constructor() { }
}
