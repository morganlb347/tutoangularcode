import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isAuth=false;
  constructor() { }

  SignIn(){
   return new Promise<boolean>((resolve, reject) => {
    setTimeout(() => {
      this.isAuth=true;
      resolve(true);
    }, 2000);
   })
  }
  SignOut(){
    this.isAuth=false;
  }
  
}
