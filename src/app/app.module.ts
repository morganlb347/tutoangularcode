import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import { AppComponent } from './app.component';
import { MonPremierComponent } from './components/mon-premier/mon-premier.component';
import { AppareilsComponent } from './components/appareils/appareils.component';
import { AppareilService } from './service/appareil.service';
import { AuthComponent } from './components/auth/auth.component';
import { AppareilviewComponent } from './components/appareilview/appareilview.component';
import { AppRoutingModule } from './app-routing.module';
import { AuthService } from './service/auth.service';
import { SingleappareilComponent } from './components/singleappareil/singleappareil.component';
import { FourOhFourComponent } from './components/four-oh-four/four-oh-four.component';
import { AuthGuardService } from './service/auth-guard.service';
import { EditAppareilComponent } from './components/edit-appareil/edit-appareil.component';
import { UserlistComponent } from './components/userlist/userlist.component';
import { NewUsersComponent } from './components/new-users/new-users.component';
import { UserService } from './service/user.service';

@NgModule({
  declarations: [
    AppComponent,
    MonPremierComponent,
    AppareilsComponent,
    AuthComponent,
    AppareilviewComponent,
    SingleappareilComponent,
    FourOhFourComponent,
    EditAppareilComponent,
    UserlistComponent,
    NewUsersComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    AppareilService,
    AuthService,
    AuthGuardService,
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
