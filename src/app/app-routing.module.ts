import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule,Routes } from '@angular/router';
import { AppareilviewComponent } from './components/appareilview/appareilview.component';
import { AuthComponent } from './components/auth/auth.component';
import { SingleappareilComponent } from './components/singleappareil/singleappareil.component';
import { FourOhFourComponent } from './components/four-oh-four/four-oh-four.component';
import { AuthGuardService } from './service/auth-guard.service';
import { EditAppareilComponent } from './components/edit-appareil/edit-appareil.component';
import { UserlistComponent } from './components/userlist/userlist.component';
import { NewUsersComponent } from './components/new-users/new-users.component';

const routes: Routes = [
  {path: 'appareils',canActivate:[AuthGuardService],component:AppareilviewComponent },
  {path:'auth',component:AuthComponent},
  {path:'',canActivate:[AuthGuardService],component:AppareilviewComponent},
  {path:'appareils/:id',canActivate:[AuthGuardService],component:SingleappareilComponent},
  {path:'notfound',component:FourOhFourComponent},
  {path:'edit',canActivate:[AuthGuardService],component:EditAppareilComponent},
  {path:'users',component:UserlistComponent},
  {path:'adduser',component: NewUsersComponent},
  {path:'**',redirectTo:'/notfound'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
