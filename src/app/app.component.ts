import { Component, OnDestroy, OnInit } from '@angular/core';
import { AppareilService } from './service/appareil.service';
import { interval, Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit,OnDestroy{
  seconde!: number;
  subcribe!:Subscription;

  constructor(){

  }
  ngOnDestroy(){
  this.subcribe.unsubscribe();
  }

  ngOnInit(){
    const counter=interval(1000);
    this.subcribe=counter.subscribe(
      (value:number)=>{
        this.seconde=value;
      },
      (error:any)=>{
        console.log("une erreur a été rencontrée");
      },
      ()=>{
        console.log("observable complétée!");
      });

  }

}
